import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import MaterialTable from 'material-table';
import axios from 'axios';
import { modificaPais } from '../../redux/actions/pais/pais';
import { connect } from "react-redux";
import { withRouter } from 'react-router'


const columnas = [
	{ title: 'Nombre del país', field: 'Country' },
	{ title: 'Total de casos confirmados', field: 'TotalConfirmed' },
	{ title: 'Total de muertes', field: 'TotalDeaths' },
	{ title: 'Total de recuperaciones', field: 'TotalRecovered' },
]

const baseUrl = 'https://api-gateway-test-2101.herokuapp.com/summary';

const TablaCovid = ({ modificaPais }) => {

	let history = useHistory();

	const redirect = (dato) => {
		modificaPais(dato)
		history.push('/historico')
	}

	const [data, setData] = useState([]);

	const peticionGet = async () => {
		await axios.get(baseUrl)
			.then(response => {
				setData(response.data)
			})
	}

	useEffect(() => {
		peticionGet();
	}, [])

	return (<div>
		<MaterialTable
			title='Listado de Casos'
			columns={columnas}
			data={data}
			actions={[
				{
					icon: 'add',
					tooltip: 'ver mas',
					onClick: (event, rowData) => { redirect(rowData.Country) }
				}
			]}
			options={{
				actionsColumnIndex: -1
			}}
			localization={{
				header: {
					actions: 'Historico'
				}
			}}
		/>
	</div>)

}


export default withRouter(connect(null, { modificaPais })(TablaCovid))

