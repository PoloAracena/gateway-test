import { createStore, combineReducers } from 'redux';
import paisReducer from '../../reducers/pais/pais';

const reducers = combineReducers({
    paisReducer
});

const store = createStore(
    reducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;