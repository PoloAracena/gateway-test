package cl.socialit.services;

import java.time.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ServicesApplication {
	@Value("${timeoutMiliseconds}")
	private int timeoutMiliseconds;
	
	private static final Logger log = LoggerFactory.getLogger(ServicesApplication.class);
		
	public static void main(String[] args) {
		SpringApplication.run(ServicesApplication.class, args);
	}
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
	    return builder
	            .setConnectTimeout(Duration.ofMillis(timeoutMiliseconds))
	            .setReadTimeout(Duration.ofMillis(timeoutMiliseconds))
	            .build();
	}

}
