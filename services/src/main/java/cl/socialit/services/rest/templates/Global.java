package cl.socialit.services.rest.templates;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Global {
	@JsonProperty("ID")
	private String ID;
	@JsonProperty("NewConfirmed")
	private int NewConfirmed;
	@JsonProperty("TotalConfirmed")
	private int TotalConfirmed;
	@JsonProperty("NewDeaths")
	private int NewDeaths;
	@JsonProperty("TotalDeaths")
	private int TotalDeaths;
	@JsonProperty("NewRecovered")
	private int NewRecovered;
	@JsonProperty("TotalRecovered")
	private int TotalRecovered;
	
	public Global() {
		
	}
}
