# README #

React Redux Router Saga Boilerplate

This repository is created to agilize the beginning of a project and of course, you are interested in using Redux Saga & React Router. That's why I created my own easy boilerplate.

I'm using these powerful libraries

* Redux - https://github.com/reduxjs/redux
* Redux Saga - https://github.com/redux-saga/redux-saga
* React Redux - https://github.com/reduxjs/react-redux 
* React Router - https://github.com/ReactTraining/react-router
* Axios - https://github.com/axios/axios
## API GATEWAY
Configurado para ejecutarse en el puerto 80.
### Endpoints
GET:

http://[127.0.0.1]/summary

http://[127.0.0.1]/country/[slug]

## SERVICIOS
Configurados para ejecutarse en el puerto 8081

Referirse a respectivos archivos properties para setear los accesos gateway-servicio.